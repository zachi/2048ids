﻿using UnityEngine;
using System.Collections;

public class GameControllerlBehaviour : MonoBehaviour
{
		public int PlayTime = 0;
		public static int Points = 0;
		public static bool GameOver = false;
		public static GameControllerlBehaviour i;

		void Awake ()
		{
				i = null;
				i = this;
		}

		void Start ()
		{
				this.PlayTime = 0;
				Points = 0;
				this.InvokeRepeating ("AddSec", 1f, 1f);

		}

		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.R)) {
						StartNewGame ();
				}
		}

		void OnGUI ()
		{
				GUILayout.BeginHorizontal ();
				GUILayout.Label ("Blocks left: " + NumberBehaviour.BlockCount + " ||  PlayTime : " + PlayTime + " Score : " + Points);
				GUILayout.EndHorizontal ();


				if (GameOver) {
						string r = "Restart";
						if (NumberBehaviour.BlockCount == 0) {		
								r = "You've won in " + PlayTime + " seconds \nRestart";
						} else {
								r = "You've wasted " + PlayTime + " seconds to earn " + Points + " points\nRestart";
						}
						if (GUI.Button (
								    new Rect (Screen.width * .25f, Screen.height * .25f, Screen.width * .5f, Screen.height * .5f), r)) {
								StartNewGame ();
						}
				}
		}

		public void StartNewGame ()
		{
				NumberBehaviour.BlockCount = 2047;
				Application.LoadLevel (0);
				Points = 0;
				GameOver = false;
		}

		public static void InvokeGameOver ()
		{
				i.CancelInvoke ();
				GameOver = true;
		}

		void AddSec ()
		{
				this.PlayTime++;
		}
}
