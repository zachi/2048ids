﻿using UnityEngine;
using System.Collections;

public class MissileBehaviour : MonoBehaviour
{
		Vector2 originalVelocity;
		// Use this for initialization
		void Start ()
		{
				this.rigidbody2D.AddRelativeForce (new Vector2 (0f, 400f));
		}

		void FixedUpdate ()
		{
				if (originalVelocity.x == 0 && this.originalVelocity.y == 0) {
						this.originalVelocity = new Vector2 (
							Mathf.Clamp (this.rigidbody2D.velocity.x, -5f, 5f),
							Mathf.Clamp (this.rigidbody2D.velocity.y, -5f, 5f)
						);
				}
				this.rigidbody2D.velocity = this.originalVelocity;
		}

		void DestroySoon ()
		{
				GameObject.Destroy (this.gameObject);
		}
}
