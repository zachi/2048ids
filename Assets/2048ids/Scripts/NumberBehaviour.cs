﻿using UnityEngine;
using System.Collections;

public class NumberBehaviour : MonoBehaviour
{
		public static int BlockCount = 2047;
		public Sprite[] sprites;
		public Color[] colors;
		public GameObject RefNumber;
		// 0 - 10 -> sprite idx
		public int Idx;

		public void Start ()
		{
				if (this.Idx == 10) {
						this.rigidbody2D.velocity = new Vector2 (
								Random.Range (-.5f, .5f),
								Random.Range (-.5f, .5f)
						);
				} else {
						this.rigidbody2D.velocity = new Vector2 (
								Random.Range (-3f, 3f),
								Random.Range (-3f, 3f));
				}
		}

		void FixedUpdate ()
		{
				this.rigidbody2D.velocity = new Vector2 (
						Mathf.Clamp (this.rigidbody2D.velocity.x, -2.5f, 2.5f),
						Mathf.Clamp (this.rigidbody2D.velocity.y, -2.5f, 2.5f)
				);
		}

		void OnCollisionEnter2D (Collision2D c)
		{
				if (c.collider.name.StartsWith ("Missile")) {
						Vector2 originalVelocity = (c.gameObject.rigidbody2D.velocity);
						GameObject.Destroy (c.collider.gameObject);
						this.audio.PlayOneShot (this.audio.clip);
						if (this.Idx > 0) {
								this.Idx--;
								this.RefNumber.GetComponent<SpriteRenderer> ().sprite = this.sprites [this.Idx];
								this.RefNumber.GetComponent<SpriteRenderer> ().color = this.colors [this.Idx];
								this.transform.localScale *= .88f;
								Vector3 nVelocity = new Vector3 (Mathf.Clamp (originalVelocity.y, -1f, 1f), Mathf.Clamp (originalVelocity.x, -1f, 1f), 0f);
								this.transform.position = this.transform.position + nVelocity * this.transform.localScale.x * .5f;
								this.gameObject.rigidbody2D.velocity = nVelocity;
								GameObject other = (GameObject)GameObject.Instantiate (this.gameObject, this.transform.position, this.transform.rotation);
								other.transform.position = this.transform.position - nVelocity * this.transform.localScale.x * .5f;
								other.rigidbody2D.velocity = -nVelocity;
								BlockCount--;
								GameControllerlBehaviour.Points++;
						} else {
								GameObject.Destroy (this.gameObject);
								BlockCount--;
								GameControllerlBehaviour.Points += 2;
						}
						
				} else if (c.collider.name.StartsWith ("Ship")) {
						GameObject.Destroy (c.collider.gameObject);
						GameControllerlBehaviour.InvokeGameOver ();
				}
		}
}
