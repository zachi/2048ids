﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(AudioSource))]
public class ShipBehaviour : MonoBehaviour
{
		public Transform spawnMissile;
		public GameObject missilePrefab;
		public bool CanShoot = true;

		void FixedUpdate ()
		{
				this.rigidbody2D.velocity *= .995f;
				if (Input.GetKey (KeyCode.LeftArrow)) {
						this.transform.RotateAround (this.transform.position,
								Vector3.forward,
								6f);
				}
				if (Input.GetKey (KeyCode.RightArrow)) {
						this.transform.RotateAround (this.transform.position,
								Vector3.forward,
								-6f);
				}
				if (Input.GetKey (KeyCode.UpArrow)) {
						this.rigidbody2D.AddRelativeForce (new Vector2 (0f, 5f));
				}
				if (Input.GetKey (KeyCode.Space) || Input.GetKey (KeyCode.X)) {
						this.Shoot ();
				}
				
				this.rigidbody2D.velocity = 
				new Vector2 (
						Mathf.Clamp (this.rigidbody2D.velocity.x, -3.7f, 3.7f),
						Mathf.Clamp (this.rigidbody2D.velocity.y, -3.7f, 3.7f)
				);
		}

		public void Shoot ()
		{
				if (CanShoot) {
						this.CanShoot = false;
						this.audio.PlayOneShot (this.audio.clip);
						GameObject.Instantiate (missilePrefab, spawnMissile.position, this.transform.rotation);
						Invoke ("UnlockShoot", .1f);
				}

		}

		public void UnlockShoot ()
		{
				this.CanShoot = true;
		}

		void OnCollisionEnter2 ()
		{
				Debug.Log ("Game Over");
		}


}
