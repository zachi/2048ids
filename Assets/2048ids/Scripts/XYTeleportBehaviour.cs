﻿using UnityEngine;
using System.Collections;

public class XYTeleportBehaviour : MonoBehaviour
{

		void FixedUpdate ()
		{
				if (Mathf.Abs (this.transform.position.x) > 5f) {
						this.TeleportX ();
				}
				if (Mathf.Abs (this.transform.position.y) > 5f) {
						this.TeleportY ();
				}
		}



		public void TeleportX ()
		{
				this.transform.position = new Vector3 (
						Mathf.Clamp (-this.transform.position.x, -5f, 5f),
						Mathf.Clamp (this.transform.position.y, -5f, 5f),
						this.transform.position.z
				);

		}

		public void TeleportY ()
		{
				this.transform.position = new Vector3 (
						Mathf.Clamp (this.transform.position.x, -5f, 5f),
						Mathf.Clamp (-this.transform.position.y, -5f, 5f),
						this.transform.position.z
				);
		}
}
